# Get Minimum value in graph

Implements a Pregel-like bulk-synchronous message-passing API.
Unlike the original Pregel API, the GraphX Pregel API factors the sendMessage computation over edges, enables the message sending computation to read both vertex attributes, and constrains messages to the graph structure. 

s mentioned earlier, graphs created by modern appliction can be very large (potentially terabytes or petabytes in size), thus, a single node (computer) will not be able to hold all these nodes in memory. The way forward is to partition and parallelise the graph to involve many machines to process them in parallel.

Pregel employs a vertex-centric approach in processing large distributed graphs. As described in this paper, Pregel computations consist of a sequence of iterations, called supersteps. During a superstep, the following can happen in the framework:

It receives and reads messages that are sent to V from the previous superstep S-1.
It applies a user-defined function f to each vertices in parallel, so f essentially specifies the behaviour of a single vertex V at a single superstep S.
It can mutate the state of V.
It can send messages to other vertices (typically along outgoing edges) that the vertices will receive in the next superstep S+1.

In this approach, all communations are between supersteps S and S+1. Within each superstep, the same user-defined function f is applied to all of the vertices in parallel.

     class Graph[VD, ED] {
         val vertices: VertexRDD[VD]
         val edges: EdgeRDD[ED]
    }

     def pregel[A]
      (initialMsg: A,
       maxIter: Int = Int.MaxValue,
       activeDir: EdgeDirection = EdgeDirection.Out)
      (vprog: (VertexId, VD, A) => VD,
       sendMsg: EdgeTriplet[VD, ED] => Iterator[(VertexId, A)],
       mergeMsg: (A, A) => A)
    : Graph[VD, ED]
	
The first argument list contains configuration parameters:

    initialMsg is the user defined message that will be sent to the vertices prior to superstep 0.
    maxIter is the maximum number of iterations we will perform before it terminates.
    activeDir is the  edge direction in which to send messages, normally, this will be EdgeDirection.Out.

The second argument list contains the functions that handles the mutation of state and message passing:

    vprog is the user defined function for receiving messages.
    sendMsg is the user defined function to determine the messages to send out for the next iteration and where to send it to.
    mergeMsg is the user defined function to merge multiple messages arriving at the same vertex at the start of a superstep before applying the vertex program vprog.
